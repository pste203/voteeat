﻿<?php
session_start();
error_reporting(E_ALL); // ces deux premières lignes autorisent les erreurs à l'écran
ini_set('display_errors', 'On');
?>

<!DOCTYPE html>
<html>
<head>

    <link rel="apple-touch-icon" sizes="57x57" href="/Icone/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/Icone/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/Icone/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/Icone/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/Icone/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/Icone/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/Icone/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/Icone/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/Icone/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/Icone/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/Icone/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/Icone/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/Icone/favicon-16x16.png">
    <link rel="manifest" href="/Icone/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <title>Vote Eat, votez pour manger !</title>
    <meta name="description" content="Regroupez vous avec vos amis et collègues pour déjeuner ensemble">
    <meta name="keywords" content="vote, eat, manger, regroupement, amis, collègue, friend, déjeuner, dîner, lunch, dinner, restaurant, food, take away, ranking, classement">
    <meta name="robots" content="index, follow" />

    <link rel="stylesheet" type="text/css" href="style.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <meta charset="UTF8"> <!-- permet les accents -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--//include ici
    //de librairie
    <script src="http://localhost/voteat/include/js/highcharts.js"></script>

    //bootstrap est télélchargé mais pas linké, dans includer


    //ou de fichiers
    -->

    <?php include("Class/ClassAdministrator.php") ?>

    <?php include("Class/ClassAnon.php") ?>

    <?php include("Class/ClassGroup.php") ?>
    <?php include("Class/ClassModerator.php") ?>
    <?php include("Class/ClassRestaurant.php") ?>
    <?php include("Class/ClassUser.php") ?>
    <?php include("Class/ClassSystem.php") ?>
    <?php include("Class/ClassMapping.php") ?>
    <?php include("Authentification/db.php") ?>

    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="style.css"/>
</head>

<body>

<div class="all">
    <div class="elem_gauche">
        <img src="bordureGauche.png">
    </div>
    <div class="bouton1">
        <a id="connect"  class="btn btn-lg btn-success" role="button"> Se connecter </a>
    </div>
    <div class="conteneur">
    <div class="logo">
        <a href="http://voteat.ovh/" ><img src="Logo.gif" ></a>
    </div>
    <div id="ZoneDAffichage">
    </div>
    </div>
    <div class="bouton2">
        <a id="subscribe" class="btn btn-lg btn-success" role="button" > S'inscrire</a>
    </div>
    <div class="elem_droite">
        <img src="bordureDroite.png">
    </div>
</div>

<!-- Gab : lourd ces 2 lignes de codes -->
<?php if ($_SERVER["REQUEST_METHOD"] != "POST") {?>
<?php }?>

<script>

    $(function(){
        $("#connect").click(
            function () {
                <?php
                $var=getLink("authentification");
                echo "var file = '{$var}';";
                ?>

                $('#ZoneDAffichage').load(file);
                $('#ZoneDAffichage').fadeToggle(0);
                $('#ZoneDAffichage').fadeToggle();
                $('html, body').animate({
                    scrollTop: $("#ZoneDAffichage").offset().top
                }, 800);

            }

        );
        $("#subscribe").click(
            function () {
                <?php
                $var=getLink("signup");
                echo "var file = '{$var}';";
                ?>

                $('#ZoneDAffichage').load(file);
                $('#ZoneDAffichage').fadeToggle(0);
                $('#ZoneDAffichage').fadeToggle();
                $('html, body').animate({
                    scrollTop: $("#ZoneDAffichage").offset().top
                }, 800);

            }
        );
    });

</script>

<?php


//Gab : je ne sais pas à quoi servent ces 10 lignes
$aMembers = [];

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>


<div id="ZoneTest">
</div>

</body>
<footer>
    <div id="foot" class="text center-block" style="width: 100%; height: 80px; background-color: #0a0a0a; font-size: 14px"><p> Vote Eat by : Gabriel Padis - Antoine Caudwell - Baptiste Gerondeau - Henri Verclytte - Benjamin Zamith - Raphaël Séguin</p>
    <p> Contact : support@voteat.ovh © VoteEat2017</p>
    </div>
</footer>
</html>