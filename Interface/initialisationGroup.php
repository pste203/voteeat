<?php

session_start();

error_reporting(E_ALL); // ces deux premières lignes autorisent les erreurs à l'écran
ini_set('display_errors', 'On');

include("../Class/ClassMapping.php");
include("../Authentification/db.php");
?>

<!DOCTYPE html>
<html>

<script>
    $(function () {
        $("#infoGroup").submit(function (event) {
            event.preventDefault();
            var $form = $(this);
            var values = {};
            values["NameGroup"] = $form.find("input[name='NameGroup']").val();
            values["Radius"] = $form.find("input[name='Radius']").val();
            values["StartTime"] = $form.find("select[name='StartTime']").val();
            values["FinishTime"] = $form.find("select[name='FinishTime']").val();
            var newpage = $.post("<?php echo getLink("initialisationGroup") ?>", {
                NameGroup: values["NameGroup"],
                StartTime: values["StartTime"],
                FinishTime: values["FinishTime"]
            });

            newpage.done(function (data) {
                <!-- alert(data); -->

                var page=$.post("run_deploy.php");
                page.done(function (data) {
                    $("#depliant").html(data);

                    $ ("#ZoneDisplayGroup").html("");
                    $('#additionalButtons').html("");
                    $("#ZoneDisplayGroup").fadeIn(0);
                    $('#additionalButtons').fadeIn(0);
                });
                $("#ZoneDAffichage").html(data);
            })
                .fail(function () {
                    alert("Post Error");
                })

        });
        $("#return").click(function(){
            $("#ZoneDAffichage").html("");
            $ ("#ZoneDisplayGroup").fadeIn();
            $('#additionalButtons').fadeIn();

        });
    });

</script>

<?php

/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 9/04/2017
 * Time: 16:00
 */

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    $data = str_replace("'", " ", $data);
    return $data;
}


$NameGroupErr = $RadiusErr = "";

$error = 0;
print_r($_SESSION);
print_r(getLink("initialisationGroup"));

if (isset($_POST["NameGroup"])) { //A BLINDER
    //echo "1";
    if (empty($_POST["NameGroup"])) {
        $NameGroupErr = "Il faut un nom pour votre groupe";
        $error = 1;
        //echo "2";
    } else {

                echo "4bis";
                echo "6";

                print_r($_POST);

                //si la localisation est bien mise : HENRI
                ?>


                <!--<script>
                function getLocation() {
                    if (navigator.geolocation)
                        navigator.geolocation.getCurrentPosition(show_map);
                }

                function show_map(pos) {

                    var Lat = pos.coords.latitude;
                    var Lon = pos.coords.longitude;
                    var LatLon = new google.maps.LatLng(Lat,Lon);

                    var mapsetting={
                        center: LatLon,
                        zoom: 15
                    };
                    map = new google.maps.Map(document.getElementById("map"), mapsetting);
                }

                google.maps.event.addListener(map, 'click', function(event) {
                    placeMarker(event.latLng);
                });

                function placeMarker(location) {
                    var marker = new google.maps.Marker({
                        map: map,
                        position: location
                    });

                    <?php
                    //$lat=location.lat();
                    //$lng=location.lng();
                    ?>
                }
                </script> -->
                <?php
                //création du groupe dans la DB avec table group les info du group

                //test input de NameGroup
                $_POST["NameGroup"] = test_input($_POST["NameGroup"]);

                    $sqlInsertGroup = "INSERT INTO groupUsers (Nom, Nbmembres) VALUES ('$_POST[NameGroup]', 1)";
                //A ajouter latitude et longitude HENRI
                $result = exec_sql($sqlInsertGroup);
                if($result) echo "La création du groupe est réussie";
                else echo "La création du groupe a échouée.";

                $sqlRecupIDGroup = "SELECT IDGroup FROM groupUsers WHERE Nom = '$_POST[NameGroup]'";
                $IDGroup = exec_sql($sqlRecupIDGroup)[0][0];
                echo "L'identifiant du groupe créer est: $IDGroup";

                //linkgrouprromuser où chaque id de groupe a au moins un pseudo un horaire debut, de fin et un bool admin(/mod?)
                if($IDGroup) {
                    $sqlInsertLink = "INSERT INTO linkgrouproomuser SET IDGroup = '$IDGroup', IDUser = '$_SESSION[ID]',Autorisation=1, StartTime = '$_POST[StartTime]', FinishTime = '$_POST[FinishTime]'";
                    $result = exec_sql($sqlInsertLink);
                    $a=NULL;
                    $b=NULL;
                    if ($_SERVER["REQUEST_METHOD"]=="POST") {
                        if(isset($_POST["StartTime"]))
                        {$a=$_POST["StartTime"].":00:00";}
                        if(isset($_POST["FinishTime"]))
                        {$b=$_POST["FinishTime"].":00:00";}
                        if($a!=NULL)
                        {
                            if($b!=NULL)
                            {
                                if($a!=$b) {
                                    exec_sql("INSERT INTO room SET IDGroup='$IDGroup',Debut='$a',Fin='$b'");
                                }
                            }
                        }
                    }
                    if($result) echo "Le nouveau lien a été créé avec succès";
                    else echo "La création du lien a échouée";
                }

                echo "Bravo le groupe '$_POST[NameGroup]' a été créer avec succès!";
            }
}

//on a fait la création du groupe

//invitation des personnes
$allEmail = [];
$allUsername = [];

if (isset($_POST["MemberEmail"])) { // si un email est écrit
    ?>
    <!-- et qu'enter est appuyé (source : http://stackoverflow.com/questions/29046077/submit-a-form-on-enter-key-press) -->
    <script>
        document.onkeydown = function (evt) {
            var keyCode = evt ? (evt.which ? evt.which : evt.keyCode) : event.keyCode;
            if (keyCode == 13) {
                //your function call here
                <?php
                array_push($allEmail, $_POST["MemberEmail"]);
                ?>
                $("#EmailPrinting").html(<?php print_r($allEmail) ?>);
            }
        }
    </script>
    <?php
}

if (isset($_POST["MemberUsername"])) { // si un Username est écrit
    ?>
    <!-- et qu'enter est appuyé (source : http://stackoverflow.com/questions/29046077/submit-a-form-on-enter-key-press) -->
    <script>
        document.onkeydown = function (evt) {
            var keyCode = evt ? (evt.which ? evt.which : evt.keyCode) : event.keyCode;
            if (keyCode == 13) {
                //your function call here
                <?php
                array_push($allUsername, $_POST["MemberUsername"]);
                ?>
                $("#UsernamePrinting").html(<?php  print_r($allUsername) ?>);
            }
        }
    </script>
    <?php
}

//sélection des restaurants

if ($error == 1 or $_SERVER["REQUEST_METHOD"] != "POST") { ?>

    <h1>Création d'un groupe</h1>
    <div class="text">
        <p><span class="error">* champs obligatoire.</span></p>

        <form id="infoGroup" method="post" action="<?php echo getLink("initialisationGroup"); ?>?submit=infoGroup">

            <!-- On ne se sert pas de ces variables mais on va mettre une carte HENRI

                <td align="right"><p>Adress</p></td>
                <input type = "text" name = "Adresse" maxlength="100" size="20">
                <span class="error">*</span> <br>

                <td align="right"><p>Zipcode</p></td>
                <input type = "text" name = "ZipCode" maxlength="100" size="20">
                <span class="error">*</span> <br>

                <td align="right"><p>City</p></td>
                <input type = "text" name = "City" maxlength="100" size="20">
                <span class="error">*</span> <br>

                -->

            <!-- Général au groupe -->

            <td align="right"><p>Nom du groupe</p></td>
            <input type="text" class="colors" name="NameGroup" maxlength="100" size="25">
            <span class="error">*<?php echo $NameGroupErr; ?></span> <br>

           <!-- <td align="right"><p>Rayon de recherche</p></td>
            <input type="text" class="colors" name="Radius" maxlength="100" size="25">
            <span class="error">*<?php echo $RadiusErr; ?></span> <br> -->


            <!-- Information personelle POUR LES HORAIRES FAIRE UN CHOIX EN LISTE ET CHANGER LES VALEURS ACCEPTER PAR LA DB-->

            <td align="right"><p>Généralement, à quelle heure pouvez vous déjeuner?</p></td>
            <p>Cette valeur sera strictement personnelle et n'engage à rien quant aux horaires du groupe.</p>
            <select name="StartTime" class="text">
                <option value=1>1h</option>
                <option value=2>2h</option>
                <option value=3>3h</option>
                <option value=4>4h</option>
                <option value=5>5h</option>
                <option value=6>6h</option>
                <option value=7>7h</option>
                <option value=8>8h</option>
                <option value=9>9h</option>
                <option value=10>10h</option>
                <option value=11>11h</option>
                <option value=12 selected>12h</option>
                <option value=13>13h</option>
                <option value=14>14h</option>
                <option value=15>15h</option>
                <option value=16>16h</option>
                <option value=17>17h</option>
                <option value=18>18h</option>
                <option value=19>19h</option>
                <option value=20>20h</option>
                <option value=21>21h</option>
                <option value=22>22h</option>
                <option value=23>23h</option>
                <option value=24>24h</option>
            </select>
            <span class="error">*</span> <br>

            <td align="right"><p>Généralement, jusqu'à quand pouvez vous déjeuner?</p></td>
            <p>Cette valeur sera strictement personnelle et n'engage à rien quant aux horaires du groupe.</p>
            <select name="FinishTime" class="text">
                <option value=1>1h</option>
                <option value=2>2h</option>
                <option value=3>3h</option>
                <option value=4>4h</option>
                <option value=5>5h</option>
                <option value=6>6h</option>
                <option value=7>7h</option>
                <option value=8>8h</option>
                <option value=9>9h</option>
                <option value=10>10h</option>
                <option value=11>11h</option>
                <option value=12>12h</option>
                <option value=13 selected>13h</option>
                <option value=14>14h</option>
                <option value=15>15h</option>
                <option value=16>16h</option>
                <option value=17>17h</option>
                <option value=18>18h</option>
                <option value=19>19h</option>
                <option value=20>20h</option>
                <option value=21>21h</option>
                <option value=22>22h</option>
                <option value=23>23h</option>
                <option value=24>24h</option>
            </select>
            <span class="error">*</span> <br> <br>

            <a id="return" type="button" class="btn btn-lg btn-success">Retour</a>
           <div class="bouton3"> <input class="btn btn-lg btn-success" type="submit" value="Créer un Groupe"> </div>
        </form>

        <!-- <h1>Invitation de membres</h1>
        <form id="membersGroup" method="post" action="<?php// echo getLink("initialisationGroup"); ?>?submit=membersGroup">

            <!-- Général au groupe -->

            <!-- <td align="right"><p>Rentrez les mails des utilisateurs que vous souhaitez inviter</p></td>
            <input type="email" name="MemberEmail" maxlength="100" size="20" class="colors">

            <td align="right"><p>Rentrez les pseudos des utilisateurs que vous souhaitez inviter</p></td>
            <input type="text" name="MemberUsername" maxlength="100" size="20" class="colors">

            <div id="EmailPrinting">
            </div>

            <div id="UsernamePrinting">
            </div>
            <br>
            <div class="bouton3"> <input class="btn btn-lg btn-success" type="submit" value="Envoyer une invitation"> </div>

        </form> -->

        <?php
}
    ?>

    <div id="Display">
    </div>
    </div>
</html>

<!--<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCtyiOb0oysFOzEMJc5k73SVJZH-29EUgY&signed_in=true&libraries=places&callback=getLocation">
</script>-->
