<?php
session_start();
include("../Authentification/db.php");
include("../Class/ClassMapping.php");
error_reporting(E_ALL); // ces deux premières lignes autorisent les erreurs à l'écran
ini_set('display_errors', 'On');
?>
<!DOCTYPE html>
<html>



<script>

    $(function () {
        $("#return").click(function(){
            $ ("#ZoneDAffichage").fadeOut(200);
            $ ("#ZoneDAffichage").html("");
            $ ("#ZoneDAffichage").fadeIn(0);
            $('#displayAdd').fadeOut(200);

        });
        $("#formRoom").submit(function (event) {
            event.preventDefault();

            var $form = $(this);
            var values = {};
            values["Debut"] = $form.find("select[name='Debut']").val();
            values["Fin"] = $form.find("select[name='Fin']").val();
            var newpage = $.post("addRoom.php", {
                Debut: values["Debut"],
                Fin: values["Fin"],
                IDGroup: <?php echo $_POST["IDGroup"];?>
            });

            newpage.done(function (data) {
                $("#displayAdd").html(data);
                $("#allpage").load("index.php");
            })
                .fail(function () {
                    alert("Post Error");
                })

        });
    });
</script>

<body >
<div class="text">
    <?php

    $ErrorMessage = "";
    $WinMsg = "";
    $error = 1;


    $a=NULL;
    $b=NULL;
    $c=$_POST["IDGroup"];
    if ($_SERVER["REQUEST_METHOD"]=="POST") {
        if(isset($_POST["Debut"]))
        {$a=$_POST["Debut"].":00:00";}
        if(isset($_POST["Fin"]))
        {$b=$_POST["Fin"].":00:00";}
        if($a!=NULL)
        {
            if($b!=NULL)
            {
                if($a!=$b) {
                    exec_sql("INSERT INTO room SET IDGroup='$c',Debut='$a',Fin='$b'");
                }
            }
        }
    }
    if ($error){ ?>
        <form id="formRoom" method="post" action="addRoom.php">
            <select name="Debut" class="text">
                <option value=1>1h</option>
                <option value=2>2h</option>
                <option value=3>3h</option>
                <option value=4>4h</option>
                <option value=5>5h</option>
                <option value=6>6h</option>
                <option value=7>7h</option>
                <option value=8>8h</option>
                <option value=9>9h</option>
                <option value=10>10h</option>
                <option value=11>11h</option>
                <option value=12 selected>12h</option>
                <option value=13>13h</option>
                <option value=14>14h</option>
                <option value=15>15h</option>
                <option value=16>16h</option>
                <option value=17>17h</option>
                <option value=18>18h</option>
                <option value=19>19h</option>
                <option value=20>20h</option>
                <option value=21>21h</option>
                <option value=22>22h</option>
                <option value=23>23h</option>
                <option value=24>24h</option>
            </select>
            <span class="error">*</span> <br>

            <select name="Fin" class="text">
                <option value=1>1h</option>
                <option value=2>2h</option>
                <option value=3>3h</option>
                <option value=4>4h</option>
                <option value=5>5h</option>
                <option value=6>6h</option>
                <option value=7>7h</option>
                <option value=8>8h</option>
                <option value=9>9h</option>
                <option value=10>10h</option>
                <option value=11>11h</option>
                <option value=12>12h</option>
                <option value=13 selected>13h</option>
                <option value=14>14h</option>
                <option value=15>15h</option>
                <option value=16>16h</option>
                <option value=17>17h</option>
                <option value=18>18h</option>
                <option value=19>19h</option>
                <option value=20>20h</option>
                <option value=21>21h</option>
                <option value=22>22h</option>
                <option value=23>23h</option>
                <option value=24>24h</option>
            </select>
            <span class="error">*</span> <br> <br>
            <div class="bouton3"> <input class="btn btn-lg btn-success" type="submit" value="Créer une Room"> </div>
            <a id="return" type="button" class="btn btn-lg btn-success">Retour</a>
        </form>
    <?php }
    echo $ErrorMessage;
    echo $WinMsg;
    if(!$error){?>
        <a id="return" type="button" class="btn btn-lg btn-success">Retour</a>

    <?php } ?>
</div>
</body>
</html>