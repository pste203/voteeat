<!DOCTYPE html>
<html style="height: 100%">
<head>
    <link rel="apple-touch-icon" sizes="57x57" href="../Icone/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../Icone/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../Icone/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../Icone/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../Icone/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../Icone/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../Icone/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../Icone/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../Icone/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="../Icone/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../Icone/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../Icone/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../Icone/favicon-16x16.png">
    <link rel="manifest" href="../Icone/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <title>Vote Eat: Accueil</title>
    <link rel="stylesheet" href="../include/css/bootstrap-slider.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../style.css"/>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    <script src="../include/js/bootstrap-slider.js"></script>


</head>
<body id="allpage" style="min-height: 100%">
<?php
include("../Class/ClassMapping.php");
include("../Authentification/db.php");
include("groupDisplay.php");
error_reporting(E_ALL); // ces deux premières lignes autorisent les erreurs à l'écran
ini_set('display_errors', 'On');

?>
<script>
    $(function () {

        $("#Groups").click(function(){
            $("#depliant").toggle();
            }

        );


        $("#infouser").click(
            function () {
                <?php
                $var = getLink("infouser");
                echo "var file = '{$var}';";
                ?>
                $('#ZoneDAffichage').html("");
                $('#ZoneDisplayGroup').fadeOut();
                $('#additionalButtons').fadeOut();
                $('#ZoneDAffichage').load("infouser.php");
                if (!$("#ZoneDAffichage").is(":visible")) $("#ZoneDAffichage").fadeToggle();
                else
                {
                    $("#ZoneDAffichage").fadeToggle(0);
                    $("#ZoneDAffichage").fadeToggle();
                }

            }
        );
    });
</script>
<div id="depliant" class="boutons_afficher_groupe">
    <?php deploy_groups(); ?>
</div>

<div class="conteneur_bis">
    <div class="menus_depliant" id="menu_depliant" style="flex-direction: row; width: 700px; justify-content: space-between ">
        <a id="Groups" class="btn btn-lg btn-success" role="button"> Mes Groupes</a>

    </div>
    <div id="additionalButtons"></div>
    <a href="http://voteat.ovh/Interface/" class="texte_sec center-block"></a>

    <div class="boutons">
        <a id="infouser" class="btn btn-lg btn-success" role="button"> Informations utilisateur</a>
    </div>

</div>


    <div id="ZoneDAffichage" class="paragraphe"></div>
    <div id="ZoneDisplayGroup"></div>


</body>
<footer>
    <div id="foot" class="text center-block" style="width: 100%; height: 80px; background-color: #0a0a0a; font-size: 14px; margin-top: 6%"><p> Vote Eat by : Gabriel Padis - Antoine Caudwell - Baptiste Gerondeau - Henri Verclytte - Benjamin Zamith - Raphaël Séguin</p>
        <p> Contact : support@voteat.ovh © VoteEat2017</p>
    </div>
</footer>
</html>

