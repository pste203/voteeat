<?php
session_start();
include("../Authentification/db.php");
include("../Class/ClassMapping.php");
error_reporting(E_ALL); // ces deux premières lignes autorisent les erreurs à l'écran
ini_set('display_errors', 'On');
?>
<!DOCTYPE html>
<html>



<script>

    $(function () {
        $("#return").click(function(){
            $ ("#ZoneDAffichage").fadeOut(200);
            $ ("#ZoneDAffichage").html("");
            $ ("#ZoneDAffichage").fadeIn(0);

            $('#ZoneDisplayGroup').fadeIn(200);
            $('#displayAdd').fadeOut(200);
            $('#addMember').fadeToggle(100);


        });
        $("#formMember").submit(function (event) {
            event.preventDefault();

            var $form = $(this);
            var values = {};
            values["Nom"] = $form.find("input[name='pseudo']").val();
            values["Admin"]= $form.find("input[name='admin']").val();
            var newpage = $.post("addMember.php", {
                Nom: values["Nom"],
                Admin: values["Admin"],
                IDGroup: <?php echo $_POST["IDGroup"];?>
            });

            newpage.done(function (data) {
                $("#displayAdd").html(data);
                var page=$.post('loadMembers.php',{IDGroup: <?php echo $_POST['IDGroup'];?>});
                page.done(function(data)
                {

                    $('#pseudos').html(data);
                });

                $('html, body').animate({
                    scrollTop: $("#displayAdd").offset().top
                }, 400);
            })
                .fail(function () {
                    alert("Post Error");
                })

        });
    });
</script>

<div id="CLebuffer"></div>
<div class="text">
    <?php

    function test_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        $data = str_replace("'", " ", $data);
        return $data;
    }

    $ErrorMessage = "";
    $WinMsg = "";
    $error = 1;
    if ($_SERVER["REQUEST_METHOD"]=="POST") {
        if(isset($_POST["Nom"])) {
            if (empty($_POST["Nom"])) {
                    $ErrorMessage = "Nom non saisi";
            } else {
                $c = $_POST["Nom"];
                $c = test_input($c);
                $IDUser = exec_sql("SELECT IDUser FROM utilisateurs WHERE Pseudo='$c'");
                if (empty($IDUser)) {
                    $ErrorMessage = "Cet utilisateur n'existe pas";
                } else {
                    $a = $_POST["IDGroup"];
                    $b = $IDUser[0][0];
                    $IDAdmin = exec_sql("SELECT Admin FROM linkgrouproomuser WHERE IDGroup='$a'");
                    $d = 0;
                    if(isset($_POST["Admin"])) $d=$_POST["Admin"];
                    exec_sql("INSERT INTO linkgrouproomuser SET IDGroup='$a',IDUser='$b',Autorisation='$d' ");
                    $WinMsg = "Personne ajoutée :" . $_POST['Nom'];
                    $error=0;

                    //+1 nombre de membres du group
                    $sql_nb = "SELECT Nbmembres FROM groupUsers WHERE IDGRoup = '$a'";
                    $nbMembres = exec_sql($sql_nb)[0][0];

                    $nbMembres = $nbMembres+1;

                    $sql_updateNb = "UPDATE groupUsers SET Nbmembres = '$nbMembres' WHERE IDGroup = '$a'";
                    exec_sql($sql_updateNb);

                }
            }
        }
    }
    if ($error){ ?>

        <form id="formMember" method="post" action="addMember.php">
            Personne à ajouter:<br>
            <input class="colors"  size="25" type="text" name="pseudo" > <br>
            <input type="checkbox" name="admin" value="2"> Modérateur <br>
            <a id="return" type="button" class="btn btn-lg btn-success">Retour</a>
            <input class="btn btn-lg btn-success" type="submit" value="Ajout">
        </form>
    <?php }
    echo $ErrorMessage;
    ?>
</div>
</html>