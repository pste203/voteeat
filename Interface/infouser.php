<?php
session_start();
include("../Authentification/db.php");
include("../Class/ClassMapping.php");
error_reporting(E_ALL); // ces deux premières lignes autorisent les erreurs à l'écran
ini_set('display_errors', 'On');
?>
<!DOCTYPE html>
<html>
<div class="paragraphe">

    <script>

        $(function () {
            $("#return").click(function () {
                $("#ZoneDAffichage").fadeOut(200);
                $("#ZoneDAffichage").html("");
                $("#ZoneDAffichage").fadeIn(0);

                $('#ZoneDisplayGroup').fadeIn(200);

            });
            $("#infoUser").submit(function (event) {
                event.preventDefault();

                var $form = $(this);
                var values = {};
                values["Nom"] = $form.find("input[name='pseudo']").val();
                values["Mail"] = $form.find("input[name='mail']").val();
                values["Ancien_mdp"] = $form.find("input[name='amdp']").val();
                values["Nouveau_mdp"] = $form.find("input[name='nmdp']").val();
                values["Renouveau_mdp"] = $form.find("input[name='nnmdp']").val();

                var newpage = $.post("infouser.php", {
                    Nom: values["Nom"],
                    Mail: values["Mail"],
                    Ancien_mdp: values["Ancien_mdp"],
                    Nouveau_mdp: values["Nouveau_mdp"],
                    Renouveau_mdp: values["Renouveau_mdp"]
                });

                newpage.done(function (data) {
                    $("#ZoneDAffichage").html(data);
                })
                    .fail(function () {
                        alert("Post Error");
                    })

            });
        });
    </script>


    <body>
    <div class="text">
        <?php

        function test_input($data)
        {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            $data = str_replace("'", " ", $data);
            return $data;
        }

        $tab = [];
        $tab = exec_sql("SELECT * From utilisateurs Where Pseudo='$_SESSION[Username]'");
        $mID = $tab[0][0];


        $ErrorMessage = "";
        $WinMsg = "";
        $error = 1;
        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            if (empty($_POST["Nom"])) {
                $ErrorMessage = "Nom non saisi";

            } else if ($tab[0][1] == $_POST["Nom"]) {


            } else {
                $error = 0;
                $WinMsg = "Pseudo modifié par " . $_POST['Nom'];

                //test input du nom
                $_POST["Nom"] = test_input($_POST["Nom"]);

                $sql1 = "SELECT COUNT(*) FROM utilisateurs WHERE pseudo = '$_POST[Nom]' ";
                if (exec_sql($sql1)[0][0] == 0) {
                    $sql1 = "UPDATE utilisateurs SET pseudo='$_POST[Nom]' WHERE IDUser= '$mID' ";
                    exec_sql($sql1);

                }
            }


            if (empty($_POST["Mail"])) {
                $ErrorMessage = "Mail non saisi";
            } else if ($tab[0][2] == $_POST["Mail"]) {


            } else {

                //test input email
                $_POST["Mail"] = test_input($_POST["Mail"]);

                $sql1 = "UPDATE utilisateurs SET Email='$_POST[Mail]' WHERE IDUser= '$mID' ";
                exec_sql($sql1);
                $error = 0;
                $WinMsg = "Email modifié";
            }


            if (empty($_POST["Nouveau_mdp"])) {
            } else if ($_POST["Nouveau_mdp"] == $_POST["Renouveau_mdp"]) {
                $error = 0;
                $hash = password_hash($_POST['Nouveau_mdp'], PASSWORD_DEFAULT);

                $sql1 = "UPDATE utilisateurs SET TempMDP='$hash' WHERE IDUser= '$mID' ";
                exec_sql($sql1);
                $WinMsg = "Mot de passe modifié";
            }


        } ?>
        <br>
        <p> Votre pseudo est :
            <?php
            $tab = exec_sql("SELECT * From utilisateurs Where Pseudo='$_SESSION[Username]'");
            echo $tab[0][1];
            ?>
        </p>


        <br>
        <p> Votre mail est :
            <?php
            echo $tab[0][2];

            ?>
        </p>


        <br>


        <?php if ($error) { ?>


            <form id="infoUser" method="post" action="infouser.php">


                Pseudo:<br>
                <input class="colors" size="25" type="text" name="pseudo" value= <?php echo $tab[0][1] ?>>
                <br>

                Mail:<br>
                <input class="colors" size="25" type="text" name="mail" value= <?php echo $tab[0][2] ?>>
                <br>


                Nouveau mot de passe :<br>
                <input class="colors" size="25" type="password" name="nmdp">
                <br>

                Confirmer le nouveau mot de passe :<br>
                <input class="colors" size="25" type="password" name="nnmdp">
                <br>

                <br>
                <a id="return" type="button" class="btn btn-lg btn-success" style="margin-left: 40px;">Retour</a>
                <input class="btn btn-lg btn-success" type="submit" value="Valider" style="margin-left: 20px;">


            </form>
        <?php }
        echo $ErrorMessage;
        echo $WinMsg;
        ?>

    </div>
    </body>
</div>
</html>