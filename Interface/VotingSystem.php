<script>
    $(function () {
            $("#return").click(function () {
                var replace=$.post("afficherGroup.php",{IDGroup: <?php echo $_POST['IDGroup'];?>});
                replace.done(function (data) {
                    $("#ZonePrincipale").fadeOut();
                   $.when( $("#ZoneOfficielleResult").fadeOut()).done(function () {
                       $(".roomButton").css("background-color",'transparent');
                       $("#CTresLeTampon").hide();
                       $("#CTresLeTampon").html(data);
                       $('#boutonsRoom') . remove();
                       $("#ZoneDisplayGroup").html($("#CTresLeTampon").html());
                   });

                });
            });
            $('#submit').click(function () {
                var id = $('.valSlider').map(function () {
                   return $(this).attr('idresto');
                }).get();
                var votes = $('.valSlider').map(function () {
                   return $(this).text();
                }).get();
                var newpage = $.post("voteSubmit.php", {IDResto: id, Note: votes, IDRoom: <?php echo $_POST['IDRoom'];?>});
                newpage.done(function(){
                    var page = $.post('afficherRoom.php', {
                        IDRoom: <?php echo $_POST['IDRoom'];?>,
                        IDGroup: <?php echo $_POST['IDGroup'];?>});

                page.done(
                    function (data) {
                        $("#CTresLeTampon").hide();
                        $("#CTresLeTampon").html(data);
                        $('#ZoneOfficielleResult').html($('#zoneResultat').html());


                        //$('#ZonePrincipale').html($("#CTresLeTampon").html());
                        $("#CTresLeTampon").remove();
                        //$('#ZoneOfficielleResult').attr('class',"resultat_box text degrade");
                        //$('#ZoneOfficielleResult').fadeOut(0);
                        //$('#ZoneOfficielleResult').fadeIn();
                        $('#ZonePrincipale').html(data);
                        $('#zoneResultat').remove();
                    }
                );
            });
            });
        }
    );

</script>
<div class="text">

    <?php
    //var_dump($_SESSION["IDGroupActuel"]);
    //var_dump($_SESSION);
    date_default_timezone_set('Europe/Paris');
    $date = date('Y-m-d', time());

    $query = "SELECT DISTINCT restaurant.IDResto,restaurant.Adresse,restaurant.nom, restaurant.Categorie, restaurant.Commentaire,  sel2.Note FROM (SELECT IDResto, Note FROM vote WHERE IDRoom=$_POST[IDRoom] AND IDUser=$_SESSION[ID] AND dateVote='$date') sel2 RIGHT JOIN (restaurant INNER JOIN (SELECT IDResto FROM linkrestogroup WHERE IDGroup=$_POST[IDGroup]) choix ON choix.IDResto=restaurant.IDResto) ON restaurant.IDResto=sel2.IDResto";
    $restaurantNames = exec_sql($query);

    //var_dump($restaurantNames);


    //on récupère l'heure de la room
    $sql_getTimeRoom = "SElECT fin FROM room WHERE IDRoom = '$_POST[IDRoom]'";
    $string = exec_sql($sql_getTimeRoom)[0][0];

    //extraction de l'heure actuelle
    $localtime_assoc = localtime(time(), true); //Recupère le temps dans un tableau associatif
    $hour = substr($string, -8, 2);
    //$hour = substr($string, 0,-6);


    if (!($restaurantNames[0][0] === NULL)) {
        $counter = 0;
        echo "<div class='ligne'>";
    foreach ($restaurantNames as $singleID) {
        $counter = $counter + 1;
    echo '<div>';
    if ($localtime_assoc["tm_hour"] > $hour) //si l'heure actuelle dépasse l'heure de fin de la room
    {
        //slider disabled
        ?>
    <input id="<?php echo $singleID[0] ?>input" type="text" data-slider-min="0" data-slider-max="10"
           data-slider-step="1" data-slider-enabled="false"
           data-slider-value="<?php if ($singleID[5] !== NULL) echo $singleID[5];
           else echo "0" ?>"/>
        <?php
    }
    else { ?>

    <input id="<?php echo $singleID[0] ?>input" type="text" data-slider-min="0" data-slider-max="10"
           data-slider-step="1" data-slider-value="<?php if ($singleID[5] !== NULL) echo $singleID[5];
    else echo "0" ?>"/>

    <?php
    }
    ?>


        <span id="<?php echo $singleID[0] ?>CurrentSliderValLabel"> : <span class='valSlider'
                                                                            idresto="<?php echo $singleID[0]; ?>"
                                                                            id="<?php echo $singleID[0] ?>SliderVal"><?php if ($singleID[5] !== NULL) echo $singleID[5];
                else echo "0" ?></span></span>


        <script>
            $("#<?php echo $singleID[0] ?>input").bootstrapSlider();
            $("#<?php echo $singleID[0] ?>input").on("slide", function (slideEvt) {
                $("#<?php echo $singleID[0] ?>SliderVal").text(slideEvt.value);
            });

        </script>


    <?php
    if (empty($singleID[4])) echo "<button class='btn btn btn-success bouton hover-button' id='$singleID[0]'  data-hover='Adresse: $singleID[1] Categorie: $singleID[3]'>$singleID[2]</button>";
    else echo "<button class='btn btn btn-success bouton hover-button' id='$singleID[0]'  data-hover='Adresse: $singleID[1] Categorie: $singleID[3] Commentaire: $singleID[4]'>$singleID[2]</button>";
echo '</div>';
    if ($counter == 2) {
        $counter = 0;
        echo "</div><div class='ligne'>";
    }

    }
    //if($counter=1) echo "<div></div>";

    echo "</div>";

    if ($localtime_assoc["tm_hour"] > $hour) //si l'heure actuelle dépasse l'heure de fin de la room
    {
    //bouton disabled
    ?>

        <button class="btn btn-success disabled">Voter</button>

        <?php
    }
    else { ?>
        <div class="center-block"><br>
            <button class='btn btn btn-success center-block' id="return">Retour</button>
            <button class='btn btn btn-success center-block' id="submit">Voter</button>
        </div>
    <?php }

    }
    ?>
</div>
