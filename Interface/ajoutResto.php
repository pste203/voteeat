<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 20/04/2017
 * Time: 14:14
 */

session_start();

error_reporting(E_ALL); // ces deux premières lignes autorisent les erreurs à l'écran
ini_set('display_errors', 'On');

include("../Class/ClassMapping.php");
include("../Authentification/db.php");
?>

<!DOCTYPE html>
<html>

<script>

    $(function () {
        $("#return").click(function(){
            $ ("#ZoneDAffichage").fadeOut(200);
            $ ("#ZoneDAffichage").html("");
            $ ("#ZoneDAffichage").fadeIn(0);
            $('#displayAdd').fadeOut(200);
            $('#addResto').fadeToggle(100);
            $('html, body').animate({
                scrollTop: $("#ZoneDAffichage").offset().top
            }, 800);
        });
        $("#ajoutResto").submit(function (event) {
            event.preventDefault();

            var $form = $(this);
            var values = {};
            values["Nom"] = $form.find("input[name='nom']").val();
            values["Type"] = $form.find("input[name='type']").val();
            values["Lieu"] = $form.find("input[name='lieu']").val();
            values["Comm"] = $form.find("textarea[name='comm']").val();


            var newpage = $.post("ajoutResto.php", {
                Nom: values["Nom"],
                Type: values["Type"],
                Lieu: values["Lieu"],
                Comm: values["Comm"],
                IDGroup: <?php echo $_POST["IDGroup"]; ?>
            });

            newpage.done(function (data) {
                var page=$.post('afficherGroup.php',{IDGroup: <?php echo $_POST['IDGroup'];?>});
                page.done(function(data)
                {
                    $ ('#ZoneDAffichage').fadeIn(0);
                    $('#ZoneDisplayGroup').html(data);
                    $('#boutonsRoom') . remove();
                    $ ('#ZoneDAffichage').fadeOut(200);
                    $ ('#ZoneDAffichage').html('');

                });

            })
                .fail(function () {
                    alert("Post Error");
                })

        });
    });
</script>

<body>
<?php

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    $data = str_replace("'", " ", $data);
    return $data;
}


$ErrorMessage = "";
$WinMsg = "";
$error = 1;
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (isset($_POST["Nom"])) {
        if (empty($_POST["Nom"])) {
            $ErrorMessage = "Nom non saisi";
        }
        else if (isset($_POST["Type"])) {
            if (empty($_POST["Type"])) {
                $ErrorMessage = "Type non saisi";
            }
            else if (isset($_POST["Lieu"])) { //les essentiels sont remplis
                if (empty($_POST["Lieu"])) {
                    $ErrorMessage = "Lieu non saisi";
                } else {
                    $error = 0;

                    $WinMsg = "Le restaurant ajouté est " . $_POST['Nom'];

                    if (isset($_POST["Comm"])) { //s'il y a un commentaire on le met avec le reste dans la requête
                        //if(($test=exec_sql("")))

                        //test input
                        $_POST["Nom"] = test_input($_POST["Nom"]);
                        $_POST["Lieu"] = test_input($_POST["Lieu"]);
                        $_POST["Type"] = test_input($_POST["Type"]);
                        $_POST["Comm"] = test_input($_POST["Comm"]);


                        $sql1 = "INSERT INTO  restaurant SET Nom='$_POST[Nom]', Adresse='$_POST[Lieu]', Categorie ='$_POST[Type]', Commentaire ='$_POST[Comm]'";
                        exec_sql($sql1);
                        $sql2= "SELECT IDResto FROM restaurant WHERE Nom='$_POST[Nom]' AND Adresse='$_POST[Lieu]' AND Categorie ='$_POST[Type]' AND Commentaire ='$_POST[Comm]'";
                        $IDResto= exec_sql($sql2)[0][0];
                        $sql1= "INSERT INTO linkrestogroup SET IDResto=$IDResto, IDGroup=$_POST[IDGroup]";
                        exec_sql($sql1);
                    }
                }
            }
        }
    }
}


if ($error) { //Le form
    ?>

    <form id="ajoutResto" id_group="<?php echo $_POST['IDGroup'];?>" method="post" action="ajoutResto.php">


        Nom:<br>
        <input class="colors" size="25" type="text" name="nom">
        <br>

        Type:<br>
        <input class="colors" size="25" type="text" name="type">
        <br>


        Lieu :<br>
        <input class="colors" size="25" type="text" name="lieu">
        <br>

        Commentaire :<br>
        <textarea class="colors" rows="10" cols="30" name="comm"></textarea>
        <br>

        <br>
        <a id="return" type="button" class="btn btn-lg btn-success">Retour</a>
        <input class="btn btn-lg btn-success" type="submit" value="Ajouter Restaurant">
    </form>
<?php }
else{ ?>
<a id="return" type="button" class="btn btn-lg btn-success">Retour</a>
<?php }
//echo $ErrorMessage;
//echo $WinMsg;
?>

</body>
</html>
