<?php
include("../Authentification/db.php");
include("../Class/ClassMapping.php");
error_reporting(E_ALL); // ces deux premières lignes autorisent les erreurs à l'écran
ini_set('display_errors', 'On');
session_start();
?>
<!DOCTYPE html>
<html>
<script>
    $(function () {
        $(".roomButton").click(
            function () {
                var page=$.post('afficherRoom.php',{IDRoom: $(this).attr('id_room'), IDGroup: <?php echo $_POST['IDGroup'];?>});
                //var page=$.post('VotingSystem.php',{IDRoom: $(this).attr('id')});
                var new_users=$.post("membresRoom.php", {IDRoom: $(this).attr('id_room'), IDGroup: <?php echo $_POST['IDGroup'];?>});
                new_users.done(function (data) {
                    $('#pseudos').fadeOut(0);
                    $('#pseudos').fadeIn();
                    $('#pseudos').html(data);
                });
                    page.done(
                        function (data) {
                            $('#ZonePrincipale').html(data);
                            if ($('#zoneResultat').length) {
                                $('#ZoneOfficielleResult').html($('#zoneResultat').html());
                                $('#zoneResultat').remove();
                                //$('#ZoneOfficielleResult').attr("style","opacity: 1;");
                                $('#ZoneOfficielleResult').attr('class', "resultat_box text degrade");
                                if (!$('#ZoneOfficielleResult').is(':visible')) $('#ZoneOfficielleResult').fadeOut(0);
                                $('#ZoneOfficielleResult').fadeIn();
                            }
                        }
                    );
                    page.fail(function () {
                        alert("Post Error");
                    });

                $(".roomButton").css("background-color",'transparent');
                $(this).css("background-color",'#d9534f');
            }
        );



        $("#addResto").click(
            function () {

                var newpage = $.post('ajoutResto.php', {IDGroup: <? echo $_POST["IDGroup"]?>});
                newpage.done(function (data) {
                    $('#displayAdd').html(data);
                    if (!$("#displayAdd").is(":visible")) $("#displayAdd").fadeToggle();
                    else
                    {
                        $("#displayAdd").fadeToggle(0);
                        $("#displayAdd").fadeToggle();
                    }
                    $("#addResto").fadeOut(200);
                    if (!$("#addMember").is(":visible")) $("#addMember").fadeIn(200);
                    if (!$("#addRoom").is(":visible")) $("#addRoom").fadeIn(200);
                    $('html, body').animate({
                        scrollTop: $("#displayAdd").offset().top
                    }, 800);
                });

            }
        );
        $("#addRoom").click(
            function () {

                var newpage = $.post('addRoom.php', {IDGroup: <? echo $_POST["IDGroup"]?>});
                newpage.done(function (data) {
                    $('#displayAdd').html(data);
                    if (!$("#displayAdd").is(":visible")) $("#displayAdd").fadeToggle();
                    else
                    {
                        $("#displayAdd").fadeToggle(0);
                        $("#displayAdd").fadeToggle();
                    }
                    $("#addRoom").fadeOut(200);
                    if (!$("#addResto").is(":visible")) $("#addResto").fadeIn(200);
                    if (!$("#addMember").is(":visible")) $("#addMember").fadeIn(200);
                });

            }
        );
        $("#Quit").click(
          function () {
              var newpage = $.post('quitGroup.php', {IDGroup: <? echo $_POST["IDGroup"]?>});
              newpage.done(function () {
                  var page=$.post("run_deploy.php");
                  page.done(function (data) {
                      $("#depliant").html(data);
                      $("#ZoneDAffichage").html("")
                      $('#additionalButtons').html("");
                      $("#ZoneDisplayGroup").html("");
                      $("#ZoneDisplayGroup").fadeIn(0);
                      $('#additionalButtons').fadeIn(0);
                  });
                  });
              });
          }  
        );



</script>
<?php
$a=$_POST["IDGroup"];
$group_members=exec_sql("SELECT IDUser FROM linkgrouproomuser WHERE IDGroup=$a");
?>
<div class="tampon">
<div class=" pseudos_box degrade" style="display: flex; flex-direction: row; ">
<div id="pseudos" class=" text pseudos "
     style="height: <?php echo (count($group_members) + 1) * 60 ?>px; ">
<?php include ("loadMembers.php");?>
</div>
</div>


<div class="container degrade center-block boite_verte">
    <div id="ZonePrincipale" class="text noms_restos" >
        <h3 style="text-align: center;" "> Vos restaurants : </h3>
        <?php
        $query="
SELECT restaurant.IDResto, restaurant.Adresse, restaurant.Nom, restaurant.Categorie, restaurant.Commentaire 
FROM restaurant 
INNER JOIN linkrestogroup 
ON restaurant.IDResto=linkrestogroup.IDResto 
WHERE linkrestogroup.IDGroup=".$_POST['IDGroup'];
        $restos=exec_sql($query);
        if($restos!==false) {
            $counter = 0;
            echo "<div class='ligne'>";
            foreach ($restos as $SingleResto) {
                $counter = $counter + 1;

                if (empty($SingleResto[4])) echo "<button class='btn btn btn-success bouton' id='$SingleResto[0]' data-hover='Adresse: $SingleResto[1] Categorie: $SingleResto[3]'>$SingleResto[2]</button>";
                else echo "<button class='btn btn btn-success bouton' id='$SingleResto[0]' data-hover='Adresse: $SingleResto[1] Categorie: $SingleResto[3] Commentaire: $SingleResto[4]'>$SingleResto[2]</button>";
                if ($counter == 3) {
                    echo "</div>";
                    echo "<div class='ligne'>";
                    $counter = 0;
                }
            }
            echo'  <img src="plus.png" id="addResto" role="button" style="margin-right: 35px; width=40px; heigth=40px; margin-left: 40px; border-radius: 7px; margin-top: 4px;">
    </div>';
            while($counter<3 and $counter!=0)
            {
                echo "<div></div>";
                $counter=$counter+1;
            }
        }
        else
        {
            echo '<img src="plus.png" id="addResto" role="button" style="margin-right: 35px; width=40px; heigth=40px; margin-left: 40px; border-radius: 7px; margin-top: 4px;">';
        }

 ?>
    </div>
</div>
    <div id="ZoneOfficielleResult" style="flex-direction: column"> </div>
</div>
<div id="displayUserInfo" class="text"></div>

<div id="boutonsRoom">
    <div class="menus_depliant" style="flex-direction: row; margin-left: 190px">
        <div class=" btn-group">
            <?php

            echo "<button data-toggle='dropdown' class='btn btn-lg btn-success dropdown-toggle text' type='button'> Horaires <span class='caret'></span></button>
<ul role='menu' class='dropdown-menu dropdown-menu-default'>";

            for ($x = 0; $x < count($rooms); $x++)
            {
                echo "<li> <a role='button' class='roomButton' id_room='".$rooms[$x][0]."' id='".$rooms[$x][0]."nroom'>" . $rooms[$x][2] . " - " . $rooms[$x][3] . "</a></li>";
            }
            echo "<li><div style='align-self: center;'> <img src='plus.png' id='addRoom' class='center-block' style='border-radius: 7px;' ></div></li>";
            echo "</ul>";
            ?>
        </div>
        <a id="Quit" class="btn btn-lg btn-success" role="button">Quitter le groupe</a>
    </div>
</div>
</div>

<div id="displayAdd" class="text paragraphe"> </div>


</html>
