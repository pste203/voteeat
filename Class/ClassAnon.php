<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 10/01/2017
 * Time: 22:10
 */

class Anon
{
    public $name; //randomly generated
    public $hours;
    public $presence; //0 or 1
    public $voting; //did vote, voting, didn't vote, don't vote

    //constructor
    function __construct(){

    }

    //destructor
    function __destruct() {
        // TODO: Implement __destruct() method.
    }


    //function
    public function vote() {

    }

    //getters
    /**
     * @return mixed
     */
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getPresence()
    {
        return $this->presence;
    }

    /**
     * @return mixed
     */
    public function getVoting()
    {
        return $this->voting;
    }

    //setters
    /**
     * @param mixed $hours
     */
    public function setHours($hours)
    {
        $this->hours = $hours;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $presence
     */
    public function setPresence($presence)
    {
        $this->presence = $presence;
    }

    /**
     * @param mixed $voting
     */
    public function setVoting($voting)
    {
        $this->voting = $voting;
    }
}

?>