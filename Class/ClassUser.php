<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 10/01/2017
 * Time: 22:14
 */

class User extends Anon
{
    public $group = array();
    public $groupRole = array();

    public $email = "";
    public $username = "";
    public $password;
    public $mute;
    public $groupNow;
    public $roleNow;

    //constructor
    function __construct(){

    }

    //destructor
    function __destruct() {
        // TODO: Implement __destruct() method.
    }


    //function
    public function createGroup() {

    }

    public function addRestaurant() {

    }

    public function writeChat(){

    }

    //getters
    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getGroupNow()
    {
        return $this->groupNow;
    }

    /**
     * @return array
     */
    public function getGroupRole()
    {
        return $this->groupRole;
    }

    /**
     * @return mixed
     */
    public function getMute()
    {
        return $this->mute;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getRoleNow()
    {
        return $this->roleNow;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    //setters

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param mixed $groupNow
     */
    public function setGroupNow($groupNow)
    {
        $this->groupNow = $groupNow;
    }

    /**
     * @param array $groupRole
     */
    public function setGroupRole($groupRole)
    {
        $this->groupRole = $groupRole;
    }

    /**
     * @param mixed $mute
     */
    public function setMute($mute)
    {
        $this->mute = $mute;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @param mixed $roleNow
     */
    public function setRoleNow($roleNow)
    {
        $this->roleNow = $roleNow;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

}
?>