<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 10/01/2017
 * Time: 21:17
 */

class Restaurant
{
    //déclaration d'une propriété
    public $name = "";
    public $adress = "";
    public $beginHours = "";
    public $grade = 0;
    public $urlLaFourchette = "";

    //à revoir position gps
    public $gpsPositionLatitude;
    public $gpsPositionLongitude;

    //à revoir image
    public $image;

    //constructor
    function __construct(){

    }

    //destructor
    function __destruct() {
        // TODO: Implement __destruct() method.
    }

    //function
    public function create() {

    }

    public function display() {

    }

    public function displayOnMap() {

    }

    public function manualAdd() {

    }

    public function autoAdd() {

    }

    //getters

    /**
     * @return string
     */
    public function getAdress()
    {
        return $this->adress;
    }

    /**
     * @return string
     */
    public function getBeginHours()
    {
        return $this->beginHours;
    }

    /**
     * @return mixed
     */
    public function getGpsPositionLatitude()
    {
        return $this->gpsPositionLatitude;
    }

    /**
     * @return mixed
     */
    public function getGpsPositionLongitude()
    {
        return $this->gpsPositionLongitude;
    }
    
    /**
    * @return int
    */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getUrlLaFourchette()
    {
        return $this->urlLaFourchette;
    }

    //setters

    /**
     * @param string $adress
     */
    public function setAdress($adress)
    {
        $this->adress = $adress;
    }

    /**
     * @param string $beginHours
     */
    public function setBeginHours($beginHours)
    {
        $this->beginHours = $beginHours;
    }

    /**
     * @param mixed $gpsPositionLatitude
     */
    public function setGpsPositionLatitude($gpsPositionLatitude)
    {
        $this->gpsPositionLatitude = $gpsPositionLatitude;
    }

    /**
     * @param mixed $gpsPositionLongitude
     */
    public function setGpsPositionLongitude($gpsPositionLongitude)
    {
        $this->gpsPositionLongitude = $gpsPositionLongitude;
    }

    /**
     * @param int $grade
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param string $urlLaFourchette
     */
    public function setUrlLaFourchette($urlLaFourchette)
    {
        $this->urlLaFourchette = $urlLaFourchette;
    }

}

?>