<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 10/01/2017
 * Time: 21:31
 */

class Group
{
    //var
    public $id;
    public $name = "";
    public $localisation = "";

    //Array
    public $resto = array();
    public $person = array();
    public $vote = array();
    public $hours = array();


    //constructor
    function __construct(){

    }

    //destructor
    function __destruct() {
        // TODO: Implement __destruct() method.
    }


    //function
    public function stats() {

    }

    public function result() {

    }

    public function notification() {

    }

    public function giveLink()
    {
        $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" . "'$id'";
        ?>
        <script>
            var text = <?php echo $actual_link ?>; //getting the text from that particular Row
            //window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
            if (window.clipboardData && window.clipboardData.setData) {
                // IE specific code path to prevent textarea being shown while dialog is visible.
                return clipboardData.setData("Text", text);

            } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
                var textarea = document.createElement("textarea");
                textarea.textContent = text;
                textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in MS Edge.
                document.body.appendChild(textarea);
                textarea.select();
                try {
                    return document.execCommand("copy");  // Security exception may be thrown by some browsers.
                } catch (ex) {
                    console.warn("Copy to clipboard failed.", ex);
                    return false;
                } finally {
                    document.body.removeChild(textarea);
                }
            }

        </script>
        <?php

    }


    //getters
    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * @return string
     */
    public function getLocalisation()
    {
        return $this->localisation;
    }

    /**
     * @return array
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @return array
     */
    public function getResto()
    {
        return $this->resto;
    }

    /**
     * @return array
     */
    public function getVote()
    {
        return $this->vote;
    }

    //setters

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param array $hours
     */
    public function setHours($hours)
    {
        $this->hours = $hours;
    }

    /**
     * @param string $localisation
     */
    public function setLocalisation($localisation)
    {
        $this->localisation = $localisation;
    }

    /**
     * @param array $person
     */
    public function setPerson($person)
    {
        $this->person = $person;
    }

    /**
     * @param array $resto
     */
    public function setResto($resto)
    {
        $this->resto = $resto;
    }

    /**
     * @param array $vote
     */
    public function setVote($vote)
    {
        $this->vote = $vote;
    }

}

?>