<?php
session_start();
?>

<!DOCTYPE html >
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../style.css"/>
</head>
<body>
<div class="signup">
    <?php include ("../Class/ClassMapping.php");
    include("db.php");
    error_reporting(E_ALL); // ces deux premières lignes autorisent les erreurs à l'écran
    ini_set('display_errors', 'On');?>
    <script>
        $(function()
        {
             $("#signup").submit(function(event)
            {
                event.preventDefault();
                var $form = $(this);
                var values = {};
                values["Username"]= $form.find("input[name='newname']").val();
                values["Password"]= $form.find("input[name='newpassword']").val();
                values["Email"]= $form.find("input[name='newemail']").val();
                var newpage = $.post("<?php echo getLink("signup") ?>",
                    { newname: values["Username"], newpassword: values["Password"], newemail: values["Email"]});

                newpage.done(function(data){
                    $("#ZoneDAffichage").html(data);
                })
                    .fail(function(){
                        alert("Post Error");
                    })

            });
        });

    </script>
    <?php
    function test_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        $data = str_replace("'", " ", $data);
        return $data;
    }


    $error=0;
    if($_SERVER["REQUEST_METHOD"]=="POST") {
        if (empty($_POST['newpassword']) or empty($_POST['newname'])
            or empty($_POST['newemail']) ) {
            $error = 1;
            $NameErr = 'One or more required fields were left blank.
                Please fill them in and try again.';
        } else {

            //vérifie l'input
            $_POST["newname"] = test_input($_POST["newname"]);

            $sql1 = "SELECT COUNT(*) FROM utilisateurs WHERE pseudo <> '$_POST[newname]' ";
            $sql2 = "SELECT COUNT(*) FROM utilisateurs";
            if (exec_sql($sql1)[0][0] != exec_sql($sql2)[0][0]) {
                $error=1;
                $NameErr='Name already used Please try again';

    // Check for existing user with the new id
            }
            else {
                //vérifie l'input
                $_POST["newemail"] = test_input($_POST["newemail"]);

                // Checks if Email is alredy used
                $sql1 = "SELECT COUNT(*) FROM utilisateurs WHERE Email <> '$_POST[newemail]' ";
                if (exec_sql($sql1)[0][0] != exec_sql($sql2)[0][0]) {
                    $error = 1;
                    $NameErr = 'Email already used Please try again';
                }
                else {
                    $hash = password_hash($_POST['newpassword'], PASSWORD_DEFAULT);
                    $sql = "INSERT INTO utilisateurs  SET
                    pseudo = '$_POST[newname]',
                    Email = '$_POST[newemail]',
                    TempMDP =  '$hash'";
                    exec_sql($sql);

                    /*
                    * EXEMPLE DE MAIL
                    *
                    */
    // Email the person.
                    $message = "Bonjour !

    Merci de vous être inscrit au site Vote Eat !
    
    Pour vous connecter allez au l'adresse suivante :

        http://voteat.ovh

    Votre nom d'utilisateur est :

         $_POST[newname]

    Vous pouvez vous connecter à tout momment et changer vos indentifiants dans votre espace personnel.
    
    Si vous avez un problème n'hésitez pas à nous contacter.
    <support@voteat.ovh>.

    L'équipe Vote Eat.
    ";

                    $headers = "From: Voteat <support@voteat.ovh>"."\r\n";
                    $headers .='Content-Type: text/plain; charset="utf-8;"'."\r\n"; // ici on envoie le mail au format texte encodé en UTF-8
                    $headers .='Content-Transfer-Encoding: 8bit;'; // ici on précise qu'il y a des caractères accentués

                    mail($_POST['newemail'], "Your Vote Eat account !",
                        $message, $headers);
                }
            }
        }
    }



    if ($error==1 or $_SERVER["REQUEST_METHOD"] != "POST")
    { ?>
    <h1>Créer son compte sur Vote Eat</h1>
    <div class="Text_Auth">
    <p><font color="orangered" size="+1"><tt><b>*</b></tt></font>
        Champs requis</p> <br>
    <form id="signup" method="post" action="<?= $_SERVER['PHP_SELF'] ?>?submit=signup">
        <table border="0" cellpadding="0" cellspacing="5">

            <tr>
                <td align="right">
                    <p>Nom</p>
                </td>
                <td>
                    <input name="newname" type="text" maxlength="100" size="25" class="colors"/>
                    <font color="orangered" size="+1"><tt><b>*</b></tt></font> <br>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <p>Adresse E-Mail</p>
                </td>
                <td>
                    <input name="newemail" type="text" maxlength="100" size="25" class="colors"/>
                    <font color="orangered" size="+1"><tt><b>*</b></tt></font> <br>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <p>Mot de Passe</p>
                </td>
                <td>
                    <input name="newpassword" type="password" maxlength="100" size="25"class="colors"/>
                    <font color="orangered" size="+1"><tt><b>*</b></tt></font> <br>
                </td>
            </tr>
            <tr>
                <td align="right" colspan="2">
                    <hr noshade="noshade"/>
                    <input type="reset" value="Reinitialisation" class="btn btn-lg btn-success"/>
                    <input type="submit" name="submitok" value="   Creation   " class="btn btn-lg btn-success"/>
                </td>
            </tr>
        </table>
    </form>
<?php
}
    else
    {
        ?>
    <p><strong>Inscription réussie !</strong></p>
    <p>Votre id d'utilisateur a été envoyé à
        <strong><?=$_POST['newemail']?></strong>, l'adresse e-mail
        que vous nous avez fourni. Pour vous connecter,
        cliquez <a href="<?php GetLink('index')?>">ici</a> pour retourner à la page de connection, et ainsi vous connecter avec vos identifiants.</p>

    <?php
    }
    if($error==1) echo $NameErr;
?>
    </div>
    </div>
    </body>
    </html>