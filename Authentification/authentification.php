<?php
session_start();
?>


<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href="../style.css"/>
<head></head>
<body id="tout">
<div class="Authentification">
    <div class="Text_Auth">
    <?php
    include("../Class/ClassMapping.php");
    include("db.php");

    error_reporting(E_ALL); // ces deux premières lignes autorisent les erreurs à l'écran
    ini_set('display_errors', 'On'); ?>

    <script>
        $(function () {
            $("#auth").submit(function (event) {
                event.preventDefault();
                var $form = $(this);
                var values = {};
                values["Username"] = $form.find("input[name='Username']").val();
                values["Password"] = $form.find("input[name='Password']").val();
                var newpage = $.post("<?php echo getLink("authentification") ?>", {
                    Username: values["Username"],
                    Password: values["Password"]
                });

                newpage.done(function (data) {
                    $("#ZoneDAffichage").html(data);
                    $('#connect').remove();
                    $('#subscribe').remove();
                })
                    .fail(function () {
                        alert("Post Error");
                    })

            });
        });


    </script>
    <?php

    error_reporting(E_ALL); // ces deux premières lignes autorisent les erreurs à l'écran
    ini_set('display_errors', 'On');

    //inclusion des fichiers de sous programmes (script en php)


    //code de la partie générale

    $UserName = $Pswd = $HashPassword = "";
    $UserNameErr = $PswdErr = $HashPasswordErr = "";
    $error = 0;

    if (isset($_POST["Username"])) {

        if (empty($_POST["Username"])) {
            $error = 1;
            $NameErr = "Un nom d'utilisateur est requis";
        } else {

            $UserName = test_input($_POST["Username"]);
            // On check s'il y a que des lettres et espaces dans le nom
            if (!preg_match("/^[a-zA-Z ]*$/", $UserName)) {
                $NameErr = "Seuls des lettres et espaces sont autorisés dans un nom d'utilisateur";
                $error = 1;
            } else
                if (empty($_POST["Password"])) {
                    $NameErr = "Un mot de passe est nécessaire";
                    $error = 1;
                } else {
                    $test = !password_verify($_POST['Password'], exec_sql("SELECT TempMDP FROM utilisateurs WHERE Pseudo = '$_POST[Username]'")[0][0]);
                    // verif du mdp
                    if ($test) {
                        $NameErr = "Mot de passe ou nom d'utilisateur invalide";
                        $error = 1;
                    } else {
                        ?>
                        <br>
                        <div class="Teve_Bold"
                        <div class="tabulation">
                        <?php
                        echo "Welcome $_POST[Username]";
                        ?>
                        </div>
                        </div>
                        <br> <br>
                        <?php
                        /*  L'utilsateur est connecté
                        On créé les variables de session correspondant
                        $_SESSION["champ"] = "";
                        */

                        $_SESSION["Username"] = $_POST['Username'];
                        $_SESSION["Email"] = exec_sql("SELECT Email FROM utilisateurs WHERE pseudo = '$_POST[Username]'")[0][0];
                        $_SESSION["ID"]= exec_sql("SELECT IDUser FROM utilisateurs WHERE pseudo='$_POST[Username]'")[0][0];
                        ?>
                 <!--       <script>
                            $(function () {
                                $("#accessAccount").click(
                                    function () {
                                        <?php
                                        $var = getLink("indexInterface");
                                        echo "var file = '{$var}';";
                                        ?>

                                        $('#ZoneDAffichage').load(file);
                                        $('#ZoneDAffichage').fadeToggle(0);
                                        $('#ZoneDAffichage').fadeToggle();


                                    }
                                );
                            });
                        </script> -->
                        <div class="bouton3">
                            <a id="accessAccount" class="btn btn-lg btn-success" role="button" href="  /Interface"> Accéder à votre compte </a>
                        </div>


                        <?php
                    }
                }
        }
    }
    //Copié collé complet de W3schools mais bon comment faire autrement
    function test_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        $data = str_replace("'", " ", $data);
        return $data;
    }

    if ($error == 1 or $_SERVER["REQUEST_METHOD"] != "POST") { ?>
        <h1>Se connecter sur Vote Eat</h1>
        <div class="Text_Auth">
                <p> <font color="orangered" size="+1"><tt><b>*</b></tt></font> Champs requis.</span></p> <br>
        <form id="auth" method="post" action="<?php echo getlink("index"); ?>?submit=auth">

            <tr>
                <td align="right"><p>Nom d'utilisateur</p></td>
                <td>
                    <input type="text" name="Username" maxlength="100" size="20" class="colors">
                    <span class="error"><font color="orangered" size="+1"><tt><b>*</b></tt></font><?php echo $UserNameErr; ?></span> <br> <br>
                </td>
            </tr>
            <tr>
                <td align="right"><p>Mot de Passe</p></td>
                <input type="password" name="Password" maxlength="100" size="20" class="colors">
                 <span class="error"><font color="orangered" size="+1"><tt><b>*</b></tt></font><?php echo $PswdErr; ?></span> <br> <br>
            </tr>
             <input type="submit" value=" Valider" class="btn btn-lg btn-success">
        </form>
    <?php }
    if ($error == 1) echo $NameErr; ?>
        </div>
</div>
</div>
</body>
</html>
